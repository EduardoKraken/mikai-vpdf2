import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import es from 'vuetify/es5/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#005dac',
        secondary: '#7cc0e9',
        terciario:'#ffbe5f',
        // accent: colors.indigo.base, // #3F51B5
        // error:  colors.red.darken1,
        // info:   colors.orange,
		    // success: colors.green.darken2,
		    // warning :  colors.teal
      },
    },
  },
  lang:{
    locales:{ es },
    current: 'es'
  }
});
