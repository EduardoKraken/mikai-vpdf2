
export default {
	methods:{
    getSeries(){
    	this.series = []
      this.$http.get('clasificacion.series').then(response=>{
        response.body.forEach(element=>{
          if(element.imagename != undefined){
            element.imagename =  element.imagename
          }
        })
        this.series = response.body
      }).catch(error=>{console.log(error)})
    },

    getSubseries(){
    	this.subseries = []
      this.$http.get('clasificacion.subseries').then(response=>{
        this.subseries = response.body
      	console.log('subseries',this.subseries)
      }).catch(error=>{console.log(error)})
    },
  }
}
