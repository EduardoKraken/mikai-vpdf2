import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Login/Home.vue'
import Login from '@/views/Login/Login.vue'

import VueJwtDecode from 'vue-jwt-decode'

import store from '@/store'
import rutas from '@/router'

Vue.use(VueRouter)

// const routes: 

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    //MODULO DE LOGIN
    { path: '/home', name: 'home' , component: Home, 
      meta: { ADMIN: true , USUARIO: true} },
    { path: '/'    , name: 'Login', component: Login , 
      meta: { libre: true }},
    
    // USUARIOS  
    { path: '/catusuarios', name: 'catusuarios',  component: ()=> import('@/views/usuarios/CatUsuarios.vue') ,
      meta: { ADMIN: true } },
    { path: '/newusuario', name: 'newusuario',  component: ()=> import('@/views/usuarios/NewUsuario.vue') ,
      meta: { ADMIN: true } },
    
    { path: '/myperfil', name: 'myperfil', component:()=> import('@/views/Login/MyPerfil.vue'), 
      meta: { ADMIN: true , USUARIO: true}} ,
      
    { path: '/registro', name: 'registro', component:()=> import('@/views/Login/Registro.vue') ,
      meta: { libre: true }} ,
    { path: '/olvidacontra', name: 'olvidacontra', component:()=> import('@/views/Login/OlvidaContra.vue') ,
      meta: { libre: true }} ,
      
    { path: '/cambiacontra/:id'  , name: 'cambiacontra'  , component:()=> import('@/views/Login/CambiaContra.vue') ,
      meta: { libre: true }},
    { path: '/activarusuario/:id', name: 'activarusuario', component:()=> import('@/views/Login/ActivarUsuario.vue'),
      meta: { libre: true}},

    { path: '/listseries', name: 'listseries', component:()=> import('@/views/Clasificaciones/listSeries.vue'),
      meta: { ADMIN: true, USUARIO:true}},


    { path: '/versubserie/:docum', name: 'versubserie', component:()=> import('@/views/Clasificaciones/verSubserie.vue'),
      meta: { ADMIN: true, USUARIO:true}},

    { path: '/versubserie2/:docum', name: 'versubserie2', component:()=> import('@/views/Clasificaciones/verSubserie.vue'),
      meta: { ADMIN: true, USUARIO:true}},
      
    // { path: '/versubserie', name: 'versubserie', component:()=> import('@/views/Clasificaciones/verSubserie.vue'),
    //   meta: { ADMIN: true, USUARIO:true}},

    { path: '/tabseries', name: 'tabseries', component:()=> import('@/views/Clasificaciones/tabSeries.vue'),
      meta: { ADMIN: true},
      children:[
        { path: '/tabseries/classeries', name: 'classeries', component:()=> import('@/views/Clasificaciones/clasSeries.vue'),
          meta: { ADMIN: true}},
        { path: '/tabseries/classubseries', name: 'classubseries', component:()=> import('@/views/Clasificaciones/clasSubseries.vue'),
          meta: { ADMIN: true}},
      ]
    },

  ]
})

router.beforeEach( (to, from, next) => {
  console.log('Entro',store.state.Login.datosUsuario)

  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.datosUsuario.nivel === 'ADMIN'){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else if(store.state.Login.datosUsuario.nivel === 'USUARIO'){
    if(to.matched.some(record => record.meta.USUARIO)){
      next()
    }
  }else{
    next({
      name: 'Login'
    })
  }
})

export default router
