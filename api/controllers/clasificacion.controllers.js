
const Clasificacion = require("../models/clasificacion.model.js");

// Crear un cliente
exports.addSerie = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Clasificacion.addSerie(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la serie"
  		})
  	else res.send(data)
  })
};

exports.getSeries = (req,res) => {
    Clasificacion.getSeries((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};



exports.addSubserie = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Clasificacion.addSubserie(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la serie"
  		})
  	else res.send(data)
  })
};

exports.getSubseries = (req,res) => {
    Clasificacion.getSubseries((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

exports.updateSubserie = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	Clasificacion.updateSubserie(req.params.idsubserie, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la subserie con el id ${req.params.idsubserie }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la subserie con el id" + req.params.idsubserie 
				});
			}
		} 
		else res.send(data);
	});
}

exports.updateSerie = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	Clasificacion.updateSerie(req.params.idserie, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la subserie con el id ${req.params.idserie }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la subserie con el id" + req.params.idserie 
				});
			}
		} 
		else res.send(data);
	});
}

exports.getPalabrascve = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Clasificacion.getPalabrascve(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

// exports.getClienteId = (req, res)=>{
//     Clientes.getClienteId(req.body,(err,data)=>{
// 			if(err)
// 				res.status(500).send({
// 					message:
// 						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
// 				});
// 				else res.send(data);
//     });
// };



	
// 	







