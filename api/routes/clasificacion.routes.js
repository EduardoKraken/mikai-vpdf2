module.exports = app => {
  const clasificacion = require('../controllers/clasificacion.controllers') // --> ADDED THIS

  app.post("/clasificacion.serie.add",  clasificacion.addSerie);   
  app.get("/clasificacion.series",      clasificacion.getSeries); 
  app.put("/clasificacion.serie.update/:idserie", clasificacion.updateSerie);


  app.post("/clasificacion.subserie.add",  clasificacion.addSubserie);   
  app.get("/clasificacion.subseries",      clasificacion.getSubseries); 
  app.put("/clasificacion.subserie.update/:idsubserie", clasificacion.updateSubserie);

  app.post("/pdf.palabras.cve", clasificacion.getPalabrascve)

  // app.get("/clasificacion.all",   clasificacion.getClientes);   

};