module.exports = app => {
  const documentos = require('../controllers/documentos.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/imagenes", documentos.addImagen);
  app.post("/pdfs", documentos.addPdf);
  app.post("/pdfs.converthtml", documentos.convertFile);
  
};