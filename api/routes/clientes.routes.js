module.exports = app => {
  const clientes = require('../controllers/clientes.controllers') // --> ADDED THIS

  app.post("/clientes.add",  clientes.addCliente);   // CREAR UN NUEVO GRUPO
  app.get("/clientes.id",    clientes.getClienteId);   
  app.get("/clientes.all",   clientes.getClientes);   
  app.put("/clientes.update/:idweb", clientes.updateCliente);

};