// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors');
const fileUpload = require('express-fileupload');

// IMPORTAR EXPRESS
const app = express();

// Rutas estaticas
app.use('/fotos', express.static('../../fotos'));
app.use('/pdfs', express.static('../../pdf'));

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
var r_users          = require('./routes/users.routes');
var r_clientes       = require('./routes/clientes.routes');
var r_correos        = require('./routes/correos.routes');
var r_clasificacion  = require('./routes/clasificacion.routes');
var r_documentos     = require('./routes/documentos.routes');

r_users(app);
r_clientes(app);
r_correos(app);
r_clasificacion(app);
r_documentos(app);

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3003, () => {
  console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
  console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
  console.log("|**************************************************************| ");
  console.log("|************ Servidor Corriendo en el Puerto 3003 ************| ");
});