const sql = require("./db.js");

// constructor
const Users = function(user) {
  this.nombre = user.nombre;
  this.apellidos = user.apellidos;
  this.empresa = user.empresa;
  this.telempresa     = user.telempresa;
  this.telmovil   = user.telmovil;
  this.email   = user.email;
  this.modmaquina    = user.modmaquina;
  this.matserie  = user.matserie;
  this.password     = user.password;
  this.estatus   = user.estatus;
};

// VARIABLES PARA QUERYS

Users.login = (loger,result) =>{
  sql.query("SELECT * FROM usuariosweb WHERE email = ? AND password = ? OR email=? AND passextra=?"
    ,[loger.email, loger.password, loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res)
  });
};

Users.validaEmail = (usuario, result) =>{
  sql.query("SELECT email FROM usuariosweb WHERE email = ?",[usuario.email],(err, res) => {
    if (err) { result(err, null);  return  }
    result(null, res)
  });
};

Users.crearUsuario = (newUser, result) => {
  sql.query("INSERT INTO usuariosweb SET ?", newUser, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("creando usuario: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

Users.cambiaEstatus = (user, result)=>{
  sql.query("UPDATE usuariosweb SET estatus=? WHERE idusuariosweb = ?", [user.estatus, user.id], (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, res);
    }
  );
};

Users.updateUsuario = (user, result)=>{
  sql.query(` UPDATE usuariosweb SET nombre = ?, apellidos = ?, telmovil = ?,email = ?, password=?,
                                     empresa=?,  telempresa=?,  modmaquina=?, matserie=?,nivel=? 
                WHERE idusuariosweb = ?`, [ user.nombre, user.apellidos, user.telmovil, user.email, user.password, 
                                            user.empresa, user.telempresa,user.modmaquina,user.matserie,user.nivel, user.id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};

Users.OlvideContra = (data,result) =>{
  sql.query(`SELECT idusuariosweb, email,nombre FROM usuariosweb WHERE email = ?`,[data.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("email: ", res);
    result(null, res);
  });
};

Users.passwordExtra = (user, result)=>{
  sql.query(` UPDATE usuariosweb SET passextra = ? WHERE idusuariosweb = ?`, [ user.codigo, user.id],(err, res) =>  {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, "Usuario actualizado correctamente");
    }
  );
};

Users.updatexId = (id,user, result)=>{
  sql.query(` UPDATE usuariosweb SET nombre = ?, apellidos = ?, telmovil = ?,email = ?, password=?,
                                     empresa=?,  telempresa=?,  modmaquina=?, matserie=?,nivel=? 
                WHERE idusuariosweb = ?`, [ user.nombre, user.apellidos, user.telmovil, user.email, user.password, 
                                            user.empresa, user.telempresa,user.modmaquina,user.matserie,user.nivel, id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};




// --- SIN USAR---->
Users.create = (newUser, result) => {
  console.log('crear un usuario', newUser);
  sql.query("INSERT INTO usuariosweb SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

Users.findById = (userid, result) => {
  console.log("Recibo", userid)
  sql.query(`SELECT * FROM usuariosweb WHERE idusuariosweb = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Users.getxEmail = (data, result) => {
  if(data.nomuser == ''){
    sql.query(`SELECT * FROM usuariosweb WHERE email = ?`,[data.email], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }else{
    sql.query(`SELECT * FROM usuariosweb WHERE email = ? OR nomuser = ?`,[data.email, data.nomuser], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }
  
};

Users.getAll = result => {
  sql.query("SELECT * FROM usuariosweb", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("users: ", res);
    result(null, res);
  });
};

Users.updateById = (id, user, result) => {
  sql.query("UPDATE usuariosweb SET email = ?, name = ?, active = ? WHERE id = ?", [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

Users.remove = (id, result) => {
  sql.query("DELETE FROM usuariosweb WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

Users.removeAll = result => {
  sql.query("DELETE FROM usuariosweb", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} user`);
    result(null, res);
  });
};

Users.activarUsuario = (idusuario, result)=>{
  sql.query( `UPDATE usuariosweb SET estatus = 2 WHERE idusuariosweb = ?`, [idusuario], (err, res) => {
      if (err) { console.log("error: ", err);  result(null, err); return;  } // SI OCURRE UN ERROR LO RETORNO
      if (res.affectedRows == 0) { result({ kind: "No encontrado" }, null); return; } // SI NO ENCUENTRO EL ID

      console.log("Se actualizo el usuario: ", { idusuario: idusuario});
      result(null, { idusuario: idusuario});
    }
  );
};

module.exports = Users;