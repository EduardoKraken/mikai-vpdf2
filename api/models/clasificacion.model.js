const sql = require("./db.js");

const { PDFDocument } = require('pdf-lib');
var pdfText = require('pdf-text');
var fs = require('fs');



// constructor
const Clasificacion = series => {
  this.idserie       = series.idserie;
  this.nomserie      = series.nomserie;
  this.nomsubserie   = series.nomsubserie;
  this.estatus       = series.estatus;
  this.imagename     = series.imagename;
  this.docum1        = series.docum1;
  this.docum2        = series.docum2;
};

Clasificacion.addSerie = (c, result) => {
	sql.query(`INSERT INTO series(nomserie,imagename,estatus)VALUES(?,?,?)`,[c.nomserie,c.imagename,c.estatus], (err, res) => {	
    if (err) {
	    console.log("error: ", err);
	    result(err, null);
	    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};


Clasificacion.getSeries = result => {
  sql.query(`SELECT idserie, nomserie, imagename, estatus FROM series`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("series: ", res);
    result(null, res);
  });
};

Clasificacion.updateSerie = (id, sub, result) => {
  sql.query(` UPDATE series SET nomserie=?,imagename=? WHERE idserie = ?`,
   [sub.nomserie,sub.imagename,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated sub: ", { id: id, ...sub });
      result(null, { id: id, ...sub });
    }
  );
};


Clasificacion.addSubserie = (c, result) => {
  sql.query(`INSERT INTO subseries(nomsubserie,idserie,estatus,docum1,docum2)VALUES(?,?,?,?,?)`,
    [c.nomsubserie,c.idserie,c.estatus,c.docum1,c.docum2], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};


Clasificacion.getSubseries = result => {
  sql.query(`SELECT sub.idsubseries, sub.nomsubserie, sub.idserie, s.nomserie, sub.docum1, sub.docum2, sub.estatus FROM subseries sub LEFT JOIN series s ON sub.idserie = s.idserie;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("series: ", res);
    result(null, res);
  });
};


Clasificacion.updateSubserie = (id, sub, result) => {
  sql.query(` UPDATE subseries SET nomsubserie=?,idserie=?,docum1=?,docum2=? WHERE idsubseries = ?`,
   [sub.nomsubserie,sub.idserie,sub.docum1,sub.docum2,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated sub: ", { id: id, ...sub });
      result(null, { id: id, ...sub });
    }
  );
};

Clasificacion.getPalabrascve = (c, result) => {
  sql.query(`SELECT * FROM indices WHERE idsubserie = ? AND numpdf = ?;`, [c.idsubserie, c.numpdf],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("series: ", res);
    result(null, res);
  });
};


module.exports = Clasificacion;