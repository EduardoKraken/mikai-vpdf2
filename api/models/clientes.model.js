const sql = require("./db.js");

// constructor
const Clientes = function(clientes) {
  this.idweb     = clientes.idweb;
  this.nomcli    = clientes.nomcli;
  this.calle     = clientes.calle;
  this.numext    = clientes.numext;
  this.colonia   = clientes.colonia;
  this.ciudad    = clientes.ciudad;
  this.estado    = clientes.estado;
  this.cp        = clientes.cp;
  this.telefono  = clientes.telefono;
  this.email1    = clientes.email1;
  this.email2    = clientes.email2;
  this.rfc       = clientes.rfc;
  this.curp      = clientes.curp;
  this.nomcomer  = clientes.nomcomer;
  this.estatus   = clientes.estatus;
  this.numint    = clientes.numint;
};

Clientes.addCliente = (c, result) => {
	sql.query(`INSERT INTO clientes(nomcli,calle,numext,colonia,ciudad,estado,cp,telefono,email1,email2,rfc,curp,nomcomer,estatus,numint)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
		[c.nomcli,c.calle,c.numext,c.colonia,c.ciudad,c.estado,c.cp,c.telefono,c.email1,c.email2,c.rfc,c.curp,c.nomcomer,c.estatus,c.numint], 
    (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};

Clientes.getClienteId = (params, result)=>{
	sql.query(`SELECT nomcli,calle,numext,colonia,ciudad,estado,cp,telefono,email1,email2,rfc,curp,nomcomer,estatus,numint 
    FROM clientes WHERE idweb=?`, [params.idweb], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};


Clientes.getClientes = result => {
  sql.query(`SELECT nomcli,calle,numext,colonia,ciudad,estado,cp,telefono,email1,email2,rfc,curp,nomcomer,estatus,numint 
    FROM clientes`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("clientes: ", res);
    result(null, res);
  });
};


Clientes.updateCliente = (id, cli, result) => {
  sql.query(` UPDATE clientes SET nomcli=?,calle=?,numext=?,colonia=?,ciudad=?,estado=?,cp=?,telefono=?,email1=?,email2=?,rfc=?,curp=?,nomcomer=?,estatus=?,numint=? 
                WHERE idweb = ?`, [cli.nomcli,cli.calle,cli.numext,cli.colonia,cli.ciudad,cli.estado,cli.cp,cli.telefono,cli.email1,cli.email2,cli.rfc,cli.curp,cli.nomcomer,cli.estatus,cli.numint,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated cli: ", { id: id, ...cli });
      result(null, { id: id, ...cli });
    }
  );
};

module.exports = Clientes;